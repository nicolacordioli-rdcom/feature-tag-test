## 1.0.1
2019-02-14T18:24:58.525Z

- #bugfix: some invisible patch

Details available [here](changelog\details\1.0.1.md)

## 1.0.0
2019-02-14T18:22:29.102Z

- #big-breaking-changes: A really nice breking change
- #fix-wrong-file: Fix broken little feature

Details available [here](changelog\details\1.0.0.md)

## 0.1.0
2019-02-14T18:09:51.337Z

- #nice-one: Some little nice feature

Details available [here](changelog\details\0.1.0.md)

## 0.0.0
2019-02-14T17:54:50.862Z

- #EXAMPLE-123: Short description of what this task accomplish
- #EXAMPLE-123: Short description of what this task accomplish

Details available [here](changelog\details\0.0.0.md)

